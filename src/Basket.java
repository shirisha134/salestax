import java.util.HashMap;
import java.util.Map;

public class Basket {
  private HashMap<Item, Integer> listOfItems;
  private Money grandTotal;
  private Money totalTax;

  public Basket() {
    this.listOfItems = new HashMap<>();
    this.grandTotal = new Money(0.0);
    this.totalTax =new Money(0.0);
  }

  public void add(Item item, Integer quantity) {
    listOfItems.put(item, quantity);
  }

  public Item remove(Item item) {
    listOfItems.remove(item);
    return item;
  }

  public Money finalAmount() {
    for (Item item : listOfItems.keySet()) {
      totalTax=totalTax.add(item.totalTaxPayable());
      grandTotal=grandTotal.add(item.getTotalCostPerItem(),listOfItems.get(item));
    }
    return grandTotal;
  }

  public Money totalTaxes() {
    return totalTax;
  }

  @Override
  public String toString() {
    StringBuilder builder=new StringBuilder();
    for (Map.Entry<Item,Integer> entry:listOfItems.entrySet()){
      builder.append(entry.getValue()).append(" ").append(entry.getKey().name).append(" - ").append(entry.getKey().getTotalCostPerItem()).append("\n");
    }
    builder.append("Sales Taxes - ").append(totalTax).append("\n");
    builder.append("Total - ").append(grandTotal).append("\n");
    return builder.toString();
  }
}
