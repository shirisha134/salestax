
public class Item {
  private static final double SALES_TAX_PERCENTAGE = 0.1;
  private static final double IMPORT_DUTIES = 0.05;

  final String name;
  private Money unitPrice;
  private boolean imported;
  private ProductType category;

  enum ProductType {
    BOOK, FOOD, MEDICINE, MISCELLANEOUS;
  }

  private Item(String name, Money price, boolean isImported, ProductType category) {
    this.name = name;
    this.unitPrice = price;
    this.imported = isImported;
    this.category = category;
  }

  static Item book(String name, Money price, boolean imported) {
    return new Item(name, price, imported, ProductType.BOOK);
  }

  static Item food(String name, Money price, boolean imported) {
    return new Item(name, price, imported, ProductType.FOOD);
  }

  static Item medicine(String name, Money price, boolean imported) {
    return new Item(name, price, imported, ProductType.MEDICINE);
  }

  static Item miscellaneous(String name, Money price, boolean imported) {
    return new Item(name, price, imported, ProductType.MISCELLANEOUS);
  }

  private boolean isImported() {
    return imported;
  }

  private boolean isExempted() {
    return !(this.category == ProductType.MISCELLANEOUS);
  }

  private Money salesTax() {
    if (!this.isExempted()) {
      return this.unitPrice.multiply(SALES_TAX_PERCENTAGE);
    }
    return new Money(0.0);
  }

  private Money importedDuties() {
    if (this.isImported()) {
      return this.unitPrice.multiply(IMPORT_DUTIES);
    }
    return new Money(0.0);
  }

  private Money totalTax() {
    Money salesTax = this.salesTax();
    Money importDuties = this.importedDuties();
    return salesTax.add(importDuties);
  }

  private Money totalCostPerItem() {
    Money totalTax = this.totalTax();
    return unitPrice.add(totalTax);
  }

  Money totalTaxPayable() {
    return totalTax();
  }

  Money getTotalCostPerItem() {
    return totalCostPerItem();
  }

  @Override
  public String toString() {
    return
        name + "\t" + totalCostPerItem();
  }
}
