import java.util.ArrayList;
import java.util.Scanner;

public class Main {
  public static void main(String[] args){
    ArrayList<Item> menu=Menu.menu();
    System.out.println("-------------------------------------------------------------------------");
    System.out.println("            Available Items In Our Grocery"                               );
    System.out.println("-------------------------------------------------------------------------");
    int count =1;
    for(Item item: menu){
      System.out.println(count + ". "+ item.toString());
      count+=1;
    }
    Scanner scanner = new Scanner(System.in);
    System.out.println("-------------------------------------------------------------------------");
    System.out.println("         Please enter the  Number of Items required"                      );
    System.out.println("-------------------------------------------------------------------------");
    int size =scanner.nextInt();
    System.out.println("         Please enter the Item Number with quantity required"             );
    System.out.println("-------------------------------------------------------------------------");

    Basket basket=new Basket();
    while (size>0){
      int indexOfItem=scanner.nextInt();
      int quantity=scanner.nextInt();
      Item item=menu.get(indexOfItem-1);
      basket.add(item,quantity);
      size-=1;
    }

    basket.finalAmount();
    System.out.println(basket.toString());
  }
}
