import java.util.ArrayList;

public class Menu {

  public static ArrayList<Item> menu(){
    ArrayList<Item> menu=new ArrayList<>();
    menu.add(Item.book("Book",new Money(12.49),false));
    menu.add(Item.miscellaneous("CD",new Money(14.99),false));
    menu.add(Item.food("Chocolate bar",new Money(0.85),false));
    menu.add(Item.food("Box of chocolates",new Money(10.00),true));
    menu.add(Item.miscellaneous("Bottle of perfume",new Money(47.50),true));
    menu.add(Item.miscellaneous("Bottle of perfume",new Money(27.99),true));
    menu.add(Item.miscellaneous("Bottle of perfume",new Money(18.99),false));
    menu.add(Item.medicine("Packet of headache pills",new Money(9.75),false));
    menu.add(Item.food("Box of imported chocolates",new Money(11.25),true));
    return menu;
  }

}
