
public class Money {
  private double value;

  Money(double value) {
    this.value = value;
  }

  Money add(Integer quantity) {
    return new Money((this.value) * quantity);
  }

  Money add(Money money, Integer quantity) {
    return new Money(Math.floor(((this.value + money.value) * quantity) * 100) / 100);
  }

  Money add(Money money) {
    return new Money(Math.floor((this.value + money.value) * 100) / 100);
  }

  Money multiply(double v) {
    return round(new Money(this.value * v));
  }

  Money round(Money money) {
    return new Money(Math.ceil(money.value * 20) / 20);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Money money = (Money) o;

    return Double.compare(money.value, value) == 0;
  }

  @Override
  public int hashCode() {
    long temp = Double.doubleToLongBits(value);
    return (int) (temp ^ (temp >>> 32));
  }

  @Override
  public String toString() {
    return "" + value;
  }

}
