import org.junit.*;

import static org.junit.Assert.assertEquals;

public class BasketTest {
  @Test
  public void shouldBeAbleToAddItemToBasket() {
    Basket basket = new Basket();
    Item item = Item.book("Book", new Money(12.05),true);
    basket.add(item, 2);
    Assert.assertEquals(item, basket.remove(item));
  }
  @Test
  public void shouldBeAbleToCalculateTotalCostOfItemsInBasket() {
    Money money = new Money(10.00);
    Item item = Item.miscellaneous("EarPhones", money, false);
    Basket basket = new Basket();
    basket.add(item, 2);

    Money actualMoney = basket.finalAmount();
    assertEquals(new Money(22.00), actualMoney);

  }

  @Test
  public void shouldApplySalesTaxOnTheTotalCostOfAItem() {
    Money money = new Money(12.05);
    Item item = Item.miscellaneous("EarPhones", money, false);
    Basket basket = new Basket();
    basket.add(item, 2);

    Money salesTax = item.totalTaxPayable();
    assertEquals(new Money(1.25), salesTax);
  }

  @Test
  public void shouldReturnTotalAmountAfterAddingSalesTaxes() {
    Money money = new Money(12.05);
    Item item = Item.miscellaneous("EarPhones", money, false);
    Basket basket = new Basket();
    basket.add(item, 2);

    Money finalAmount = basket.finalAmount();
    assertEquals(new Money(26.6), finalAmount);

  }

  @Test
  public void shouldBeAbleToAddImportTaxForImportedItem() {
    Money importedPerfumeCost = new Money(47.50);
    Item item = Item.miscellaneous("Perfume", importedPerfumeCost, true);
    Basket basket = new Basket();
    basket.add(item, 1);

    Money finalAmount = basket.finalAmount();
    assertEquals(new Money(54.65), finalAmount);

  }

  @Test
  public void shouldBeAbleToAddImportTaxForImportedItems() {
    Money importedPerfumeCost = new Money(50);
    Item item = Item.miscellaneous("Perfume", importedPerfumeCost, true);
    Item item1 = Item.medicine("medicine1", new Money(60), true);
    Basket basket = new Basket();
    basket.add(item, 1);
    basket.add(item1, 1);

    Money finalAmount = basket.finalAmount();
    assertEquals(new Money(120.5), finalAmount);

  }

  @Test
  public void shouldBeAbleToAddImportTaxForImportedItemsAndNonImportedItems() {
    Money importedPerfumeCost = new Money(47.50);
    Item item = Item.miscellaneous("Perfume", importedPerfumeCost, true);
    Item item1 = Item.miscellaneous("EarPhone", new Money(47.50), false);
    Basket basket = new Basket();
    basket.add(item, 1);
    basket.add(item1, 1);

    Money finalAmount = basket.finalAmount();
    assertEquals(new Money(106.9), finalAmount);

  }

  @Test
  public void shouldBeAbleToExemptTaxOnNonImportedBooks() {
    Item item = Item.book("You Can Win", new Money(206.5), false);
    Basket basket = new Basket();
    basket.add(item, 1);

    Money finalAmount = basket.finalAmount();
    assertEquals(new Money(206.5), finalAmount);
  }

  @Test
  public void shouldBeAbleToHaveOnlyImportedDutiesOnBooks() {
    Item item = Item.book("Harry Potter", new Money(303.56), true);
    Basket basket = new Basket();
    basket.add(item, 1);

    Money finalAmount = basket.finalAmount();
    assertEquals(new Money(318.76), finalAmount);
  }

  @Test
  public void shouldNotHaveAnyTaxesOnFood() {
    Item item = Item.food("chocolates", new Money(102.8), false);
    Basket basket = new Basket();
    basket.add(item, 2);

    assertEquals(new Money(205.6), basket.finalAmount());
  }

  /*  1 book at 12.49
      1 music CD at 14.99
      1 chocolate bar at 0.85
      Sales Taxes - 1.50 
      Total - 29.83
  */
  @Test
  public void shouldBeAbleToCalculateFinalAmountOfGivenItemList1() {
    Item item1 = Item.book("Book", new Money(12.49), false);
    Item item2 = Item.miscellaneous("Music CD", new Money(14.99), false);
    Item item3 = Item.food("chocolate bar", new Money(0.85), false);
    Basket basket = new Basket();
    basket.add(item1, 1);
    basket.add(item2, 1);
    basket.add(item3, 1);

    Money finalAmount = basket.finalAmount();
    assertEquals(new Money(1.50), basket.totalTaxes());
    assertEquals(new Money(29.83), finalAmount);
  }

  /*
  Input 2
	•	1 imported box of chocolates at 10.00
	•	1 imported bottle of perfume at 47.50
	  Sales Taxes - 7.65 
	  Total - 65.15
   */
  @Test
  public void shouldBeAbleToCalculateFinalAmountOfGivenItemList2() {
    Item item1 = Item.food("Box Of chocolates", new Money(10.00), true);
    Item item2 = Item.miscellaneous("bottle of perfume", new Money(47.50), true);
    Basket basket = new Basket();
    basket.add(item1, 1);
    basket.add(item2, 1);

    Money finalAmount = basket.finalAmount();
    assertEquals(new Money(7.65), basket.totalTaxes());
    assertEquals(new Money(65.15), finalAmount);
  }

  /*
      •	1 imported bottle of perfume at 27.99
      •	1 bottle of perfume at 18.99
      •	1 packet of headache pills at 9.75
      •	1 box of imported chocolates at 11.25
      Sales Taxes - 6.70 
      Total - 74.68
   */
  @Test
  public void shouldBeAbleToCalculateFinalAmountOfGivenItemList3() {
    Item item1 = Item.miscellaneous("imported bottle of perfume", new Money(27.99), true);
    Item item2 = Item.miscellaneous("bottle of perfume", new Money(18.99), false);
    Item item3 = Item.medicine("packet of headache pills", new Money(9.75), false);
    Item item4 = Item.food("box of imported chocolates", new Money(11.25), true);
    Basket basket = new Basket();
    basket.add(item1, 1);
    basket.add(item2, 1);
    basket.add(item3, 1);
    basket.add(item4, 1);

    Money finalAmount = basket.finalAmount();
    assertEquals(new Money(6.69), basket.totalTaxes());
    assertEquals(new Money(74.66), finalAmount);
  }
}
