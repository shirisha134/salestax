import org.junit.*;

public class ItemTest {

  @Test
  public void shouldBeToCalculateSalesTaxOnEligibleItems(){
    Item item = Item.book("book1",new Money(12.05),false);
    Assert.assertEquals(new Money(12.05),item.getTotalCostPerItem());
  }
}
