import org.junit.*;

import static org.junit.Assert.*;

public class MoneyTest {
  @Test
  public void shouldBeAbleToAddToOneMoneyWithOther() {
    Money money = new Money(10.05);
    Money anotherMoney = new Money(1.625);
    assertEquals(new Money(11.67), money.add(anotherMoney));
  }

  @Test
  public void shouldBeAbleToAddGivenMoneyForGivenTimes() {
    Money money = new Money(10.05);
    assertEquals(new Money(40.2), money.add(4));
  }

  @Test
  public void shouldBeAbleCalculateSalesTax() {
    Money money = new Money(10.05);
    assertEquals(new Money(1.05), money.multiply(0.1));
  }

  @Test
  public void shouldBeAbleToRoundUpToOneDecimalPlaces() {
    Money money = new Money(1.88);
    assertEquals(new Money(1.9), money.round(money));
  }
}
